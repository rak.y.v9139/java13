package homework3_2;

import java.util.List;

public class Main {
    private static List<Book> books;
    private List<Person> persons;

    public static void main(String[] args) {
        Library library = new Library();
        library.addBook("Толстой", "Война и мир");
        library.addBook("Стругацкий", "Пикник на обочине");
        library.addBook("Толстой", "Война и мир");
        library.addBook("Перумов", "Гибель богов");
        library.addBook("Перумов", "Война мага");
        library.addBook("Перумов", "Череп на рукаве");
        library.statusBook();
        System.out.println();

        library.deleteBook("война и мир");
        library.statusBook();
        System.out.println();

        library.searchBooksAuthor("Перумов");
        System.out.println();

        library.newPerson("Саша", "Петров");
        library.newPerson("Алексей", "Нарышкин");
        library.newPerson("Петр", "Слобода");
        library.newPerson("Дмитрий", "Скворцов", "Пикник на обочине");
        library.newPerson("Иван", "Иванов", "Пикник на обочине");

        library.personTakeBook(0, "Череп на рукаве");
        library.personTakeBook(2, "Пикник на обочине");
        library.personTakeBook(3, "Череп на рукаве");
        library.personTakeBook(3, "Пикник на обочине");

        library.returnBookInLibrary(3, "Пикник на обочине", 7);
        library.personTakeBook(3, "Пикник на обочине");
        library.returnBookInLibrary(3, "Жизнь", 2);
        library.personTakeBook(3, "Пикник на обочине");
        System.out.println();
        library.statusPerson();
        System.out.println();
        library.statusBook();

    }
}