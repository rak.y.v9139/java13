package homework3_2;
/*
В текущем домашнем задании вам предстоит творческая работа — реализовать
библиотеку (с хранением книжек, возможностью одалживать книги и другими
подходящими фишками). Всё, что не указано в требованиях явно, можно решить и
придумать самостоятельно (в том числе можно дополнительно добавлять свои поля и
методы).
Каждая из задач 1-7 оценивается в 1 балл. 8 задача дополнительная (необязательная)
и оценивается в 2 балла при условии, что правильно решены первые 7 задач. Для
зачета (от 70%) достаточно решить первые 4 задачи (не думать о механизме
одалживания вообще) и написать к ним тесты.
Итак, Петя решил открыть библиотеку… Но ему нужна ваша помощь!
Должен быть реализован класс Книга, содержащий название и автора. Также должен
быть реализован класс Посетитель, содержащий имя посетителя и идентификатор
(null до тех пор пока не возьмет книгу). Должен быть реализован класс Библиотека со
следующим функционалом:
Работа со списком существующих книг в библиотеке.
Сюда входят все добавленные книги, в том числе и одолженные. Название книги
считается уникальным, и в библиотеке не может быть двух книг с одинаковым
названием.
1. Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет
в библиотеке. Если книга в настоящий момент одолжена, то считается, что она
всё равно есть в библиотеке (просто в настоящий момент недоступна).
2. Удалить книгу из библиотеки по названию, если такая книга в принципе есть в
библиотеке и она в настоящий момент не одолжена.
3. Найти и вернуть книгу по названию.
4. Найти и вернуть список книг по автору.
Механизм одалживания книги посетителю. Каждый посетитель в один момент времени
может читать только одну книгу.
5. Одолжить книгу посетителю по названию, если выполнены все условия:
a. Она есть в библиотеке.
b. У посетителя сейчас нет книги.
c. Она не одолжена.
Также если посетитель в первый раз обращается за книгой — дополнительно
выдать ему идентификатор читателя.
6. Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу. Не
принимать книгу от другого посетителя.
a. Книга перестает считаться одолженной.
b. У посетителя не остается книги.
Тестирование.
7. В методе main написать несколько тестов на реализованный функционал.
Дополнительная задача*:
8. Добавить функционал оценивания книг посетителем при возвращении в
библиотеку. Оценка книги рассчитывается как среднее арифметическое оценок
всех посетителей, кто брал эту книгу. Реализовать метод, возвращающий
оценку книги по её наименованию.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Library {
    final String BOOK_AVAILABLE = "Доступна для выдачи";
    final String BOOK_UNAVAILABLE = "Книгу уже взяли";
    private final List<Book> books = new ArrayList<>(); //Массив книг
    private final List<Person> persons = new ArrayList<>();//Массив зарегестрированных читателей


    public void addBook(String bookAuthor, String nameBook) { //Добавляем книгу в библиотеку
        Book book = new Book(bookAuthor, nameBook);
        if ((books).size() == 0) {         // Если книг в библиотеке еше нет то добавляем
            book.setBookStatus(BOOK_AVAILABLE);  //Для добавленной книги уставнавливаем статус , что книга доступна
            books.add(book);
        } else {
            for (Book p : books) {
                if (p.getNameBook().equalsIgnoreCase(book.getNameBook())) { //Проверяем есть ли уже такая книга в библиотеке
                    System.out.println("Эта книга уже есть");
                    break;
                } else {
                    books.add(book);
                    book.setBookStatus(BOOK_AVAILABLE);
                    break;
                }
            }
        }
    }

    public void deleteBook(String nameBook) {// Удаляем книгу из библиотеки
        Iterator<Book> iterator = books.iterator();
        while (iterator.hasNext()) {
            Book book = iterator.next();
            if (book.getNameBook().equalsIgnoreCase(nameBook) && book.getBookStatus().equals(BOOK_AVAILABLE)) {
                iterator.remove(); // Удаляем книгу если она находиться в библиотеке и доступна для выдачи
            }
        }
    }

    public void searchBooksAuthor(String authorName) { // Ищем все книги автора которые есть в библиотеке
        for (Book book : books) {
            if (book.getBookAuthor().equalsIgnoreCase(authorName)) {
                System.out.println(book.getNameBook());
            }
        }
    }

    public Book getSearchBooks(String bookName) {  // Ищем книгу по названию и возвращаем ее
        Book searchBook = null;
        for (Book book : books) {
            if (book.getNameBook().equalsIgnoreCase(bookName)) {
                searchBook = book;
            }

        }
        return searchBook;
    }

    public void newPerson(String name, String surname) { // Добавляем нового пользователя , и присваем ему персональный номер
        Person person = new Person(name, surname);
        persons.add(person);
        System.out.println("Ваш персональный номер: " + person.getPersonalId());
    }

    public void newPerson(String name, String surname, String bookName) { //Добавляем нового пользователя , и можем ему передать книгу
        Person person = new Person(name, surname);    // Если она доступна
        persons.add(person);   //Добавляем пользователя в библиотек , вне зависимости от результата
        for (Book book : books) {
            if (book.getNameBook().equalsIgnoreCase(bookName)) {
                if (book.getBookStatus().equals(BOOK_AVAILABLE)) {
                    book.setBookStatus(BOOK_UNAVAILABLE);
                    person.setBookPerson(book.getNameBook());
                    System.out.println("Ваш персональный номер: " + person.getPersonalId());
                } else {
                    System.out.println("Эта книга занята. " + "Вам присвоен персональный номер: " + person.getPersonalId());
                }
            }
        }
    }

    public void personTakeBook(int personalId, String bookName) { //Читатель берет книгу , по своему персональному номеру
        if (persons.get(personalId).getBookPerson() == null) { //Проверям есть ли у мользователя книга
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    if (book.getBookStatus().equals(BOOK_AVAILABLE)) { //Если книга доступна для выдачи
                        persons.get(personalId).setBookPerson(book.getNameBook()); //Читателю присваем название книги
                        book.setBookStatus(BOOK_UNAVAILABLE);  // У книги меняем статус
                    } else {
                        System.out.println("Эта книга занята. Возьмите другую.");
                        break;
                    }
                }
            }
        } else
            System.out.println("У вас уже есть книга.");
    }

    public void returnBookInLibrary(int personalId, String bookName) { // Читатель возвращает книгу в библиотеку
        if (persons.get(personalId).getBookPerson().equalsIgnoreCase(bookName)) { //Смотрим есть ли у читателя эта книга
            persons.get(personalId).setBookPerson(null); //Устанавливаем значение null для читателя
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    book.setBookStatus(BOOK_AVAILABLE);//Книга снова доступна для выдачи
                }

            }
        } else {
            System.out.println("Вы не брали эту книгу.");
        }
    }

    public void returnBookInLibrary(int personalId, String bookName, int grade) { //Читатель дополнительно может оценить книгу

        if (persons.get(personalId).getBookPerson().equalsIgnoreCase(bookName)) {
            persons.get(personalId).setBookPerson(null);
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    book.setBookStatus(BOOK_AVAILABLE);
                    book.addGrade(grade);
                }

            }
        } else {
            System.out.println("Вы не брали эту книгу.");
        }
    }

    // Методы для тесто , верно ли отображаються значения
    public void statusBook() {
        for (Book book : books) {
            System.out.println(book.getBookAuthor() + " " + book.getNameBook() + " " + book.getBookStatus() + " " + book.getAverageGrade());

        }

    }

    public void statusPerson() {
        for (Person person : persons) {
            System.out.println(person.getName() + " " + person.getSurname() + " " + person.getPersonalId() + " " + person.getBookPerson());

        }

    }
}

