package homework3_2;

import java.util.ArrayList;

public class Book {  //Класс книга
    private String bookStatus;
    private String nameBook;
    private String bookAuthor;
    private final ArrayList<Integer> grades = new ArrayList<>();  //Массив который хранит оценки читателей

    Book(String bookAuthor, String nameBook) { //Создаем новую книгу
        this.bookAuthor = bookAuthor;
        this.nameBook = nameBook;
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public String getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(String bookStatus) {
        this.bookStatus = bookStatus;
    }



    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public void addGrade(int grade) {  //Добавляет оценку читателя
        if (grade >= 0) {
            grades.add(grade);
        }
    }

    public double getAverageGrade() {  //Возвращет среднюю оценку книги
        double sum = 0;
        for (int i = 0; i < grades.size(); i++) {
            sum += grades.get(i);
        }
        return sum / (grades.size());
    }

}
