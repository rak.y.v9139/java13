package homework3_2;

public class Person {
    private static int idNumber;  // Счетчик созданых читателей
    private int personalId;    //Уникальный id каждого читателя , при создании объекта присваеться
    private String name;
    private  String surname;
    private String bookPerson; //Название книги которую взял читатель
    Person(String name, String surname){
        this.name = name;
        this.surname = surname;
        this.personalId = idNumber;
        idNumber++;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getPersonalId() {
        return personalId;
    }

    public void setBookPerson(String bookPerson) {
        this.bookPerson = bookPerson;
    }

    public String getBookPerson() {
        return bookPerson;
    }
}
