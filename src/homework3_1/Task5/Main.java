package homework3_1.Task5;

import java.util.ArrayList;
import java.util.List;




public class Main {

    public static void main(String[] args) {
       List<DayOfWeek> dayOfWeeks = new ArrayList<>();
       dayOfWeeks.add(new DayOfWeek((byte) 1,"Monday"));
       dayOfWeeks.add(new DayOfWeek((byte) 2,"Tuesday"));
       dayOfWeeks.add(new DayOfWeek((byte) 3,"Wednesday"));
       dayOfWeeks.add(new DayOfWeek((byte) 4,"Thursday"));
       dayOfWeeks.add(new DayOfWeek((byte) 5,"Friday"));
       dayOfWeeks.add(new DayOfWeek((byte) 6,"Saturday"));
       dayOfWeeks.add(new DayOfWeek((byte) 7,"Sunday"));
        for (DayOfWeek dayOfWeek : dayOfWeeks) {
            System.out.println(dayOfWeek.getDayId() + " " + dayOfWeek.getDayName());

        }

    }
}
