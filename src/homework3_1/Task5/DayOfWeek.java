package homework3_1.Task5;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */

public class DayOfWeek {
    private byte dayId;
    private String dayName;

    DayOfWeek(byte dayId, String dayName) {
        this.dayId = dayId;
        this.dayName = dayName;

    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayId(byte dayId) {
        this.dayId = dayId;
    }

    public byte getDayId() {
        return dayId;
    }
}

