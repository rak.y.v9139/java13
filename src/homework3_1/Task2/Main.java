package homework3_1.Task2;
/*
 Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)

 */

import homework3_1.Task3.StudentService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Петр", "Петров", new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
        Student student1 = new Student("Александр", "Белкин", new int[]{12, 10, 3, 4, 17, 2, 15, 25, 18, 21});
        Student student2 = new Student("Иван", "Семин", new int[]{13, 15, 20, 19, 18, 23, 18, 2, 3, 5, 45, 23});
        Student student3 = new Student("Максим" , "Соловьев" , new int[]{5 , 6 , 8} );
        students.add(student);
        students.add(student1);
        students.add(student2);
        students.add(student3);
        System.out.println(student.average());
        System.out.println(student1.average());
        System.out.println(student2.average());
        System.out.println(student3.average());
        student3.newGrade(19);
        student2.newGrade(21);
        StudentService.bestStudent(students);
        StudentService.sortBySurename(students);


        for (int i = 0; i < 4; i++) {
            System.out.print(students.get(i) + " ");

        }
    }
}
