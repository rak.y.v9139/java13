package homework3_1.Task1;
/*
Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
● sleep() — выводит на экран “Sleep”
● meow() — выводит на экран “Meow”
● eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */
import java.util.Random;

public class Cat {
    private static final int NUMBER_FUNCTION = 3;


    private static void sleep() {
        System.out.println("Sleep");
    }

    private static void meow() {
        System.out.println("Meow");
    }

    private static void eat() {
        System.out.println("Eat");
    }

    public static void status() {
        Random x = new Random();
        switch (x.nextInt(NUMBER_FUNCTION)) {
            case 0:
                sleep();
                break;
            case 1:
                meow();
                break;
            case 2:
                eat();
                break;
        }
    }

}
