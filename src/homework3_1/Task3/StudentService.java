package homework3_1.Task3;

import homework3_1.Task2.Student;
import homework3_1.Task2.Student.SureNameComparator;
/*
. Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
 */

import java.util.List;

public class StudentService {
    public static void bestStudent(List<Student> students) {       //Передаем массив класа Student
        double bestAverege = students.get(0).average();            //Перемная котороя хранит лучшее значение average
        String bestStudent = students.get(0).getName();            //Переменна для хранения имени студента
        for (int i = 1; i < students.size(); i++) {
            if (bestAverege < students.get(i).average()) {
                bestAverege = students.get(i).average();
                bestStudent = students.get(i).getName();
            }
        }
        System.out.println(bestStudent + " " + bestAverege);       //Выводим на экран имя студента и лучшее значение average


    }

    public static void sortBySurename(List<Student> students) {    //Сортируем массив по фамилии при помощи Компаратора
        students.sort(new SureNameComparator());                   //Такую запись мне предложила IDE
        //      Collections.sort(students, new SureNameComparator());    //Так я написал изначально

    }
}
