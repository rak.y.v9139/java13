package homework3_1.Task6;

/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
 */

public class AmazingString {
    private final char[] charArry;

    public AmazingString(char[] arrchar) {
        this.charArry = arrchar;
    }

    public AmazingString(String arr) {     //Переводим строку в массив char
        this.charArry = arr.toCharArray();
    }

    public void returnElementArr(int numberEltment) {                       //Возврщаем i-тый символ строки
        if (numberEltment >= 0 && numberEltment < this.charArry.length) {   //До концйа не понял надо ли вывести его на экран или вернуть
            System.out.println(charArry[numberEltment]);
        } else {
            System.out.println("Вы ввели не корректное значение");
        }
    }

    public int lengthArr() {        //Возращаем длину строки
        return this.charArry.length;
    }

    public void printArr() {         //Выводим строку на экра
        for (char arr : this.charArry) {
            System.out.print(arr);
        }
        System.out.println();
    }

    public boolean newArrEqualCharrArr(char[] newChar) { // Выглядит как плохая шутка ) , я уверен ,что есть решение лучше
        boolean charNew = false;
        char space = ' ';
        char comma = ',';
        for (int i = 0; i < this.charArry.length; i++) {
            if (newChar[0] == this.charArry[i]) {          //В основном массиве ищем первый символ нового массива
                if (newChar[0] == this.charArry[i]) {
                    for (int j = 0; j < newChar.length; j++) {   //Проверям совпадает ли последовательность символов
                        if (newChar[j] == this.charArry[i]) {
                            i++;
                            if (i == this.charArry.length) {      // если символы совпали и счетчик i стал равен длинне массива
                                if (j == newChar.length - 1) {    // то проверяем длину совпаших символов
                                    charNew = true;
                                }
                            } else {
                                if (j == newChar.length - 1) {     //Если совпали все элементы нового массива и i  не равна длине массива
                                    if (this.charArry[i] == space || this.charArry[i] == comma) {  //проверяем следующую ячейку , если она содержит
                                        charNew = true;                                           // зяпятую или пробел означающие конец слова
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return charNew;
    }


    public boolean newStringEqualCharArr(String newString) {  // Продолжение плохой шутки
        char[] newCharArr = newString.toCharArray();
        boolean charNew = false;
        char space = ' ';
        char comma = ',';
        for (int i = 0; i < this.charArry.length; i++) {
            if (newCharArr[0] == this.charArry[i]) {
                if (newCharArr[0] == this.charArry[i]) {
                    for (int j = 0; j < newCharArr.length; j++) {
                        if (newCharArr[j] == this.charArry[i]) {
                            i++;
                            if (i == this.charArry.length) {
                                if (j == newCharArr.length - 1) {
                                    charNew = true;
                                }
                            } else {
                                if (j == newCharArr.length - 1) {
                                    if (this.charArry[i] == space || this.charArry[i] == comma) {
                                        charNew = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return charNew;
    }


    public void removSpaceArr() {                 //Удаляем ведущие пробельные символы
        char space = ' ';                         // пробельный символ
        int count = 0;                            //Переменная счетчик чтобы узнать с когда заканчиватся пробелы
        for (int i = 0; i < this.charArry.length; i++) {
            if (this.charArry[i] != space) {
                count = i;
                break;
            }
        }
        for (int i = count; i < this.charArry.length; i++) { //Выводим на экран строку без ведущих пробельныех символов
            System.out.print(this.charArry[i]);
        }
        System.out.println();

    }

    public void reversPrinArr() {  //Разворачиваем строку
        for (int i = this.charArry.length - 1; i >= 0; i--) {
            System.out.print(this.charArry[i]);
        }
        System.out.println();
    }

}

