package homework3_1.Task8;

public class Atm {
    static int countInstans;                //Переменная счетчик для подсчета созданых инстансов
    private double dollarExchangeRate;
    private double rubelsExchangeRate;

   public Atm(double dollarExchangeRate , double rubelsExchangeRate){  //Создаем конструктор в которм задаем значения ждя курса валют
       if (dollarExchangeRate > 0 && rubelsExchangeRate > 0){          //Проверяем чтобы значение было положительное
           this.dollarExchangeRate = dollarExchangeRate;
           this.rubelsExchangeRate = rubelsExchangeRate;
       }else {
           System.out.println("Введите положительное значение");
       }
       countInstans++;
    }


    public void converDollarsInRubels(double dollars){  //Переводим доллары в рубли по курсу доллара
        System.out.println(dollars * this.dollarExchangeRate);
    }

    public void converRubelsInDollars(double rubels){ //Переводим рубли в доллары в рубли по курсу рублей
        System.out.println(rubels * rubelsExchangeRate);
    }

    public static int returCountInstans(){   //Возврщаем значение созданых инстансов
        return countInstans;
    }
}


