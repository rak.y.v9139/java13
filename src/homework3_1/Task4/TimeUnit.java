package homework3_1.Task4;
/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
Java 12 Базовый модуль Неделя 6
ДЗ 3 Часть 1
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному
 */

public class TimeUnit {
    private final int SECONDS_IN_MINUTES = 60;
    private final int MINUTES_IN_HOURS = 60;
    private  final int HOURS_IN_DAY = 24;
    private int minutes;
    private int hours;
    private int seconds;


    TimeUnit(int hours, int minutes, int seconds) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        this.minutes = (minutes >= 0 && minutes <= 59 ? minutes : 0);
        this.seconds = (seconds >= 0 && seconds <= 59 ? seconds : 0);

    }

    TimeUnit(int hours, int minutes) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        this.minutes = (minutes >= 0 && minutes <= 59 ? minutes : 0);
        this.seconds = 0;
    }

    TimeUnit(int hours) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        this.minutes = 0;
        this.seconds = 0;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = (hours >= 0 && hours <= 23 ? hours : 0);
        if (hours < 0 || hours > 23) {
            System.out.println("Вы ввели некоректное число, значение hours равно 0");
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = (minutes >= 0 && minutes <= 59 ? minutes : 0);
        if (minutes < 0 || minutes > 59) {
            System.out.println("Вы ввели некоректное число, значение minutes равно 0");
        }

    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = (seconds >= 0 && seconds <= 59 ? seconds : 0);
        if (seconds < 0 || seconds > 59) {
            System.out.println("Вы ввели некоректное число, значение seconds равно 0");
        }
    }
    //Выводим время  в формате hh:mm:ss
    public void setTime() {
        System.out.printf("%02d:%02d:%02d \n", this.hours, this.minutes, this.seconds);
    }
    //Выводим время  в формате hh:mm:ss am/pm
    public void timeMeridian() {
        if (this.hours > 12) {
            System.out.printf("%02d:%02d:%02d pm \n", this.hours - 12, this.minutes, this.seconds);
        } else {
            System.out.printf("%02d:%02d:%02d am \n", this.hours, this.minutes, this.seconds);

        }
    }
    //Прибавляем переданное время к установленному
    public void sumTimeUnit(int hours, int minutes, int seconds){
        this.seconds = this.seconds + seconds;
        this.minutes = this.minutes + minutes;
        this.hours = this.hours + hours;
        if (this.seconds >= SECONDS_IN_MINUTES){
            this.minutes += this.seconds / SECONDS_IN_MINUTES;
            this.seconds = this.seconds % SECONDS_IN_MINUTES;
        }
        if (this.minutes >= MINUTES_IN_HOURS){
            this.hours += this.minutes / MINUTES_IN_HOURS;
            this.minutes = this.minutes % MINUTES_IN_HOURS;
        }
        if (this.hours >= HOURS_IN_DAY){
            this.hours = this.minutes % HOURS_IN_DAY;
        }
    }


}

