package homework3_4.Task_3;

import java.io.*;
import java.util.Scanner;
/*
Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
./output.txt текст из input, где каждый латинский строчный символ заменен на
соответствующий заглавный. Обязательно использование try с ресурсами.
 */

public class Main {
    public static void newTxtFile(String name) {

        try (BufferedReader readeInput = new BufferedReader(new FileReader(name));
             PrintWriter outputWrite = new PrintWriter(new File("output.txt"))) {
            File outputFile = new File("output.txt");
            if (outputFile.exists())
                outputFile.createNewFile();
            String line = readeInput.readLine();
            outputWrite.println(line.toUpperCase());

        } catch (IOException e) {
            System.out.println("Error" + e);
        }
    }
public static void writeInput(String file){
        Scanner scanner = new Scanner(System.in);
        try(PrintWriter writeInput = new PrintWriter(new File(file))) {
            writeInput.println(scanner.nextLine());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
}

    public static void main(String[] args) {
        try  {
            File file = new File("input.txt");
            if (file.exists())
                file.createNewFile();
            writeInput(file.getName());
            newTxtFile(file.getName());
        } catch (IOException e) {
            System.out.println("Error" + e);

        }


    }
}
