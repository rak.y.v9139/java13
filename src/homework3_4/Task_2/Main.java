package homework3_4.Task_2;

import java.util.Scanner;
/*
Создать собственное исключение MyUncheckedException, являющееся
непроверяемым.
 */

public class Main {
    public static void oddNumber(int a) throws MyUncheckedException {
        if (a % 2 == 0) {
            throw new MyUncheckedException("Вы ввели четное число");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введи нечетное число:");
        int number = scanner.nextInt();
        oddNumber(number);

    }
}
