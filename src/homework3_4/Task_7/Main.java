package homework3_4.Task_7;

import java.util.Arrays;
import java.util.Scanner;
/*
На вход подается число n и массив целых чисел длины n.
Вывести два максимальных числа в этой последовательности.
Пояснение: Вторым максимальным числом считается тот, который окажется
максимальным после вычеркивания первого максимума.

 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int lengthArray = scanner.nextInt();
        int[] arr = new int[lengthArray];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        Arrays.sort(arr);
        for (int i = arr.length - 1; i > arr.length - 3; i--) {
            System.out.print(arr[i] + " ");

        }
    }
}
