package homework3_4.Task_8;

import java.util.Arrays;
import java.util.Scanner;

/*
На вход подается число n, массив целых чисел отсортированных по
возрастанию длины n и число p. Необходимо найти индекс элемента массива
равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
вывести -1.
Решить задачу за логарифмическую сложность.

 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int lengthArray = scanner.nextInt();
        int [] arr = new int[lengthArray];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        int p = scanner.nextInt();
        System.out.println(
        Arrays.binarySearch(arr,p));

    }
}
