package homework3_4.Task_4;
/*
 Создать класс MyEvenNumber, который хранит четное число int n. Используя
исключения, запретить создание инстанса MyPrimeNumber с нечетным числом.
 */

public class Main {
    public static void main(String[] args) {
        try {
            MyEvenNumber myEvenNumber = new MyEvenNumber(4);
            MyEvenNumber myEvenNumber1 = new MyEvenNumber(4);
        } catch (PositivExeption e) {
            throw new RuntimeException(e);
        }
    }
}
