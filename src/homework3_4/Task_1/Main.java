package homework3_4.Task_1;

import java.util.Scanner;
/*
Создать собственное исключение MyCheckedException, являющееся
проверяемым.
 */

public class Main {

    public static void positiveNumber(int number) throws MyCheckedException{
        Scanner scanner = new Scanner(System.in);
        while (true){
        if (number < 0){
            throw new MyCheckedException("Вводите только положительные числа");
        }else {
            System.out.print(" Попробуйте еще раз: " );
            number = scanner.nextInt();
        }

    }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int number = scanner.nextInt();
        try {
            positiveNumber(number);
        }catch (MyCheckedException e) {
            throw new RuntimeException(e);
        }
    }
}

