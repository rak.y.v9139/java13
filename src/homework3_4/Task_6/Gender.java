package homework3_4.Task_6;

public enum Gender {
    MALE("MALE"), FEMALE("FEMALE");
    private final String translation;

    Gender(String translation){
        this.translation = translation;
    }
    public String getTranslation(){
        return translation;
    }

}
