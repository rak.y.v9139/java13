package homework3_4.Task_6;
import java.text.ParseException;
import java.util.Scanner;

import static homework3_4.Task_6.FormValidator.checkBirthdate;
import static homework3_4.Task_6.FormValidator.checkGender;
import static homework3_4.Task_6.FormValidator.checkHeight;
import static homework3_4.Task_6.FormValidator.checkName;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите ваще имя: ");
        String name = scanner.next();
        checkName(name);
        System.out.println("Введите дату вашего рождения в формате dd.MM.yyyy : ");
        String date = scanner.next();
        checkBirthdate(date);
        System.out.println("Введите ваш гендер Male , Female :");
        String gender = scanner.next();
        checkGender(gender);
        System.out.println("Введите ваш рост: ");
        String height = scanner.next();
        checkHeight(height);
        System.out.println("Молодец ");


    }
}

