package homework3_4.Task_6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {

    public static void checkName(String str) { //Проверяем что бы имя начиналось с Заглавной буквы. И длина была не меньше 2-х ссимволо и не больше 20
        String regexTest = "[A-ZА-Я][a-zа-я]{1,19}\\b";
        boolean result = str.matches(regexTest);
        if (!result)
            throw new NameExeption("Имя введено не корректно");
    }

    public static void checkBirthdate(String str) throws ParseException { //Проверяем дату рождения
        Date realTime = new Date();// Актуальная дата
        Date date = new Date(0, 1, 1);// Дата 01.01.1900
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date doc = format.parse(str);
        if (!(date.before(doc) && doc.before(realTime))) //Проверяем чтобы дата была в этом промежутке
            throw new DataExeption("Дата не корректна");
    }

    public static void checkGender(String str)  {// Проверям гендер
        Gender gender = Gender.MALE;
        Gender gender1 = Gender.FEMALE;
        if (!(str.equalsIgnoreCase(gender1.getTranslation()) || str.equalsIgnoreCase(gender.getTranslation())))
            throw new GenderExeption("Не корректный ввод");
    }

    public static void checkHeight(String str) { //Проверяем рост
        double height = Double.parseDouble(str);
        if (height < 0)
            throw new HiegthNegativeExeption("Отрицательное значение");

    }

}
