package homework3_7.Task3;

import java.util.stream.Stream;
/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */

public class Main {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("abc" , "", "", "", "def","qqq");
        int number = (int) stream
                .filter(x -> x.length() > 0)
                .count();
        System.out.println("Количество непустых строк " + number);
    }

}
