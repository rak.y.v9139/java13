package homework3_7.Task5;

import java.util.List;
import java.util.stream.Stream;
/*
На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ
 */

public class Main {
    public static void main(String[] args) {
        List<String> list = Stream.of("abc", "def", "qqq").toList();
        list
                .forEach(x -> {
                    if (!x.equals(list.get(list.size()- 1))) {
                        System.out.print(x.toUpperCase() + ", ");
                    }else
                        System.out.println(x.toUpperCase() + ".");
                });


    }
}
