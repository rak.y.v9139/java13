package homework3_7.Task1;

import java.util.ArrayList;
import java.util.List;
/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */

public class Main {
    public static void main(String[] args) {
        int x = 100;
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <=100 ; i++) {
            list.add(i);
        }
        int sum = list.stream()
                .filter(y -> (y % 2 == 0))
                .mapToInt(y -> y).sum();
        System.out.println(sum);

    }
}
