package homework3_7.Task7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false
 */



public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String s1 = bufferedReader.readLine();
        String s2 = bufferedReader.readLine();

        System.out.println(calculateDistance(s1, s2));

    }
// Расстояние Левенштейна (редакционное расстояние, дистанция редактирования)
// — метрика, измеряющая по модулю разность между двумя последовательностями символов.
// Она определяется как минимальное количество односимвольных операций (а именно вставки, удаления, замены),
// необходимых для превращения одной последовательности символов в другую.
    public static boolean calculateDistance(CharSequence source, CharSequence target) {
        if (source == null || target == null) {
            throw new IllegalArgumentException("Parameter must not be null");
        }
        int sourceLength = source.length();
        int targetLength = target.length();
        if (sourceLength == 0) return false;
        if (targetLength == 0) return false;
        int[][] dist = new int[sourceLength + 1][targetLength + 1];
        for (int i = 0; i < sourceLength + 1; i++) {
            dist[i][0] = i;
        }
        for (int j = 0; j < targetLength + 1; j++) {
            dist[0][j] = j;
        }
        for (int i = 1; i < sourceLength + 1; i++) {
            for (int j = 1; j < targetLength + 1; j++) {
                int cost = source.charAt(i - 1) == target.charAt(j - 1) ? 0 : 1;
                dist[i][j] = Math.min(Math.min(dist[i - 1][j] + 1, dist[i][j - 1] + 1), dist[i - 1][j - 1] + cost);
                if (i > 1 &&
                        j > 1 &&
                        source.charAt(i - 1) == target.charAt(j - 2) &&
                        source.charAt(i - 2) == target.charAt(j - 1)) {
                    dist[i][j] = Math.min(dist[i][j], dist[i - 2][j - 2] + cost);
                }
            }
        }
        return dist[sourceLength][targetLength] == 1; // Если эквивалентно одной операции, то возвращает true
    }
}





//    public static boolean testingString(String s1, String s2) {
//        Map<Integer, Character> map = new TreeMap<>();
//        Map<Integer, Character> map2 = new TreeMap<>();
//        char[] chars = s1.toCharArray();
//        char[] chars1 = s2.toCharArray();
//
//        for (int i = 0; i < chars.length; i++) {
//            map.put(i, chars[i]);
//        }
//        for (int i = 0; i < chars1.length; i++) {
//            map2.put(i, chars1[i]);
//        }
//
//        if (map.size() == map2.size()){
//
//        }
//        return false;
//    }


//    public static boolean testingString(String s1, String s2) {
//        List<String> list = new ArrayList<>();
//        for (int i = 0; i < s1.length(); i++) {
//            list.add(s1.substring(i, i + 1));
//        }
//
//        List<String> list1 = new ArrayList<>();
//        for (int i = 0; i < s2.length(); i++) {
//            list1.add(s2.substring(i, i + 1));
//        }
//
//        if (list.size() == list1.size()) {  //Если длина равна
//            for (int i = 0; i < list.size() - 1; i++) {
//                if (!list.get(i).equals(list1.get(i))) {
//                    list.set(i, list1.get(i)); // Заменяем элемент коллекции
//                    return list.equals(list1); // возвращаем значение сравнения
//                }
//
//            }
//        } else {
//            if (list.size() > list1.size()) {  // Если первая строка больше второй
//                for (int i = 0; i < list1.size(); i++) {
//                    if (!list.get(i).equals(list1.get(i))) {
//                        list1.add(i, list.get(i)); // Добавляем элемент в коллекцию
//                        return list.equals(list1); // возвращаем значение сравнения
//                    }
//                }
//                list1.add(list.get(list.size() - 1)); // Добавляем значение в конец коллекции Строки №2
//
//            } else {   // Если вторая строка больше первой , все тоже самое только удаляем
//                for (int i = 0; i < list.size(); i++) {
//                    if (!list1.get(i).equals(list.get(i))) {
//                        list1.remove(i);
//                        return list1.equals(list);
//
//                    }
//
//                }
//                list1.remove(list1.size() - 1);
//            }
//            return list.equals(list1);
//        }
//        return false;
//    }





