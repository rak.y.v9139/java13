package homework3_7.Task6;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
/*
Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
 */

public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> sets = new HashSet<>();
        Set<Integer> sets2 = sets.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

    }
}
