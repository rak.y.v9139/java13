package homework3_7.Task4;


import java.util.Arrays;
import java.util.List;
/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */
public class Main {
    public static void main(String[] args) {
        List<Double>list = Arrays.asList(4.2,0.3,1.1,2.5,-2.7,-5.9);
        list.stream()
                .sorted((number1 , number2) -> (int) (number1 - number2))
                .forEach(number -> System.out.print(number + " "));

    }
}
