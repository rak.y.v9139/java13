package homework3_7.Task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/*
На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).

 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer>  list = new ArrayList<>();
        list.add(scanner.nextInt());
        list.add(scanner.nextInt());
        list.add(scanner.nextInt());
        list.add(scanner.nextInt());
        list.add(scanner.nextInt());
        int sum = list.stream()
                .reduce((x1 , x2) -> x1 * x2).get();
        System.out.println(sum);
    }
}
