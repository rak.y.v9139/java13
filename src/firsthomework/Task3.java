package firsthomework;
import java.util.Arrays;
import java.util.Scanner;

/*
Решить задачу 7 основного дз за линейное время.

На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.

 */

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        int[] arr2 = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr2[i] = arr[i] * arr[i];

        }
        sort2(arr2);
        for (int i : arr2) {
            System.out.print(i + " ");

        }
    }


    public static void sort(int[] arr) {
        int i = 0;
        int tmp = 0;
        while (i < arr.length - 1) {
            if (arr[i + 1] < arr[i]) {
                tmp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = tmp;
                i = 0;
            } else i++;
        }
    }

    public static void sort2(int[] arr) {
        int key = 0;
        for (int i = 0; i < arr.length; i++) {
            if (key < arr[i]){
                key = arr[i];
            }
        }
        int[] count = new int[key + 1];

        for (int i = 0; i < arr.length; i++) {
            count[arr[i]] = count[arr[i]] + 1;
        }

        int arrayIndex = 0;
        for (int i = 0; i < count.length ; i++) {
            for (int j = 0; j < count[i]; j++) {
                arr[arrayIndex] = i;
                arrayIndex++;

            }

        }



    }
}
