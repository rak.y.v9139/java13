package firsthomework;
/*
Условия следующие:
Компьютер «загадывает» (с помощью генератора случайных чисел) целое
число M в промежутке от 0 до 1000 включительно. Затем предлагает
пользователю угадать это число. Пользователь вводит число с клавиатуры.
Если пользователь угадал число M, то вывести на экран "Победа!". Если
введенное пользователем число меньше M, то вывести на экран "Это число
меньше загаданного." Если введенное число больше, то вывести "Это число
больше загаданного." Продолжать игру до тех пор, пока число не будет отгадано
или пока не будет введено любое отрицательное число.
 */

import java.util.Random;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        game();
    }

    public static void game() {
        Scanner scanner = new Scanner(System.in);
        int number = randomNumber();
        System.out.println("Число загадано.");
        int person = scanner.nextInt();
        while (person > 0) {
            if (person < number) {
                System.out.println("Это число меньше загаданного.");
                person = scanner.nextInt();
            } else if (person > number) {
                System.out.println("Это число больше загаданного.");
                person = scanner.nextInt();
            } else {
                System.out.println("Победа!");
                break;
            }
        }
    }

    public static int randomNumber() {
        Random x = new Random();
        return x.nextInt(1000) + 1;
    }
}

