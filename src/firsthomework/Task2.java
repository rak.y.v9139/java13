package firsthomework;
import java.util.Random;
import java.util.Scanner;
/*
) Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы
● строчные латинские символы
● числа
● специальные знаки(_*-)

 */

public class Task2 {
    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";
    private static final String OTHER_CHAR = "_*-";
    private static final String ALL_CHAR = CHAR_LOWER + CHAR_UPPER + NUMBER + OTHER_CHAR;


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        while (n < 8) {
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
            n = scanner.nextInt();
        }
        System.out.println(randomPassword(n));

    }

    public static String randomPassword(int x) {
        String password = "";
        do {
            password = "";
            for (int i = 0; i < x; i++) {
                Random r = new Random();
                int c = (int) (r.nextInt(ALL_CHAR.length()));
                password += ALL_CHAR.charAt(c);
            }
        } while (!password.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-_*]).{8,}"));
        return password;
    }

}

