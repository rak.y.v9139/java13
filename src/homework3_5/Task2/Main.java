package homework3_5.Task2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
 */

public class Main {
    public static Map fillsMap(Map map, char[] arr) { // Метод заполняющий Мар значениями

        for (Character character : arr) {   //Цикл заполнят Мар  по ключу чар и значению 0
            int count = 0;
            map.put(character, count);
        }
        for (Character character : arr) { // Каждый раз когда встречаем одинаковый чар увеличиваем значение на 1
            int x = (int) map.get(character);
            map.put(character, ++x);
        }
        return map; // Возвращаем заполненную Мар значениями
    }

    public static boolean searchAnagram(String s, String t) {
        if (s.length() != t.length())  //Если длинна слов (фразы) разные, то дальше не делаем проверку
            return false;
        char[] arrS = s.toCharArray();
        char[] arrT = t.toCharArray();
        Map<Character, Integer> mapS = new HashMap<>();
        Map<Character, Integer> mapT = new HashMap<>();
        return fillsMap(mapS,arrS).equals(fillsMap(mapT,arrT)); //При помощи метода сравниваем наши Мар ,возвращаем tru либо false

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.nextLine();
        System.out.println(searchAnagram(t, s));
    }
}
