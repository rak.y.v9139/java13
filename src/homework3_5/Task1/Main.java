package homework3_5.Task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.

 */

public class Main {
    public static <T> Set<T> returnCollection(ArrayList<T> arrayList){
        return new HashSet<>(arrayList);
    }

    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("лом");
        arrayList.add("дом");
        arrayList.add("ком");
        arrayList.add("лом");
        arrayList.add("лось");
        arrayList.add("кусь");
        arrayList.add("кусь");
        System.out.println(returnCollection(arrayList));

    }
}
