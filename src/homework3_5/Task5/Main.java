package homework3_5.Task5;

import java.util.*;
/*
Реализовать метод, который принимает массив words и целое положительное число k.
Необходимо вернуть k наиболее часто встречающихся слов..
Результирующий массив должен быть отсортирован по убыванию частоты
встречаемого слова. В случае одинакового количества частоты для слов, то
отсортировать и выводить их по убыванию в лексикографическом порядке.
 */
public class Main {

    public static String[] sortWords(String[] arr, int x) {
        Map<String, Integer> sortMap = new TreeMap<>(Collections.reverseOrder()); //Создаем карту в которой будут храниться количество повторений солов, но в обратном порядке
        Set<Integer> arrInt = new TreeSet<>(Collections.reverseOrder()); // Тут храним все повторения которые были в Map
        String[] backArr = new String[x]; //Массив который мы возвращаем
        for (String obj : arr) {  // Заполняем Мар ключами со значениями равными 0
            int count = 0;
            sortMap.put(obj, count);
        }

        for (String obj : arr) { // Считаем количество повторений
            int count = sortMap.get(obj);
            sortMap.put(obj, ++count);
        }

        for (Map.Entry<String, Integer> asd : sortMap.entrySet()) { //Сохраняем все значения в коллекцию , мы знаем теперь сколько раз повторялись слова
            arrInt.add(asd.getValue());
        }

        //Этот блок мне не нравиться самому, но воспаленный мозг не смог придумать ничего лучше, буду благодарен за подсказку

        int countX = 0; //Переменная для добавления значений в возвращаемый массив
        for (Integer obj : arrInt) {// Цикл по массиву повторений
            for (Map.Entry<String, Integer> count : sortMap.entrySet()) {
                if (obj.equals(count.getValue()) && countX < backArr.length) { // Ищем в Мар значения, и записываем
                    backArr[countX] = count.getKey();
                    countX++;
                }
            }
        }
        return backArr;
    }


    public static void main(String[] args) {
        String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"  };
        int x = 3;
        String[] newWords;
        newWords = sortWords(words, x);
        for (String obj : newWords) {
            System.out.print(obj + " ");
        }

    }

}

