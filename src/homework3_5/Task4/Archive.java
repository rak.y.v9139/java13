package homework3_5.Task4;

import java.util.ArrayList;
/*
В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
public class Document {
public int id;
public String name;
public int pageCount;
}
Реализовать метод со следующей сигнатурой:
public Map<Integer, Document> organizeDocuments(List<Document> documents)
 */


public class Archive { //Допустим, что это старый архив
    private final ArrayList<Document> archiveDocument = new ArrayList<>();


    public void addArchiveDocument(Document document) {
        archiveDocument.add(document);
    }


    public  ArrayList getArchiveDocument() {
        return archiveDocument;
    }


}
