package homework3_5.Task4;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Archive archive = new Archive();
        NewArchive newArchive = new NewArchive();

        Document document1 = new Document(1,"Иван",1);
        Document document2 = new Document(3,"Сергей",2);
        Document document3 = new Document(6,"Александр",3);
        archive.addArchiveDocument(document1);
        archive.addArchiveDocument(document2);
        archive.addArchiveDocument(document3);

        newArchive.organizeDocuments(archive.getArchiveDocument());



        System.out.println(newArchive.toString());
    }
}
