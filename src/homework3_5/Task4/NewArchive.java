package homework3_5.Task4;

import java.util.*;

public class NewArchive { //Новый Архив
    private final Map<Integer, Document> newArchive = new HashMap<>();



    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        for (Document dok : documents) {
            newArchive.put(dok.getId(), dok);

        }
        return newArchive;
    }

    @Override
    public String toString() {
        return "NewArchive{" +
                "newArchive=" + newArchive +
                '}';
    }
}
