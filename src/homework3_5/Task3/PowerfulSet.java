package homework3_5.Task3;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class PowerfulSet {

    public  <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> newSet = new TreeSet<>(set1);
        newSet.retainAll(set2);
        return newSet;
    }

    public  <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> newSet = new HashSet<>(set1);
        newSet.addAll(set2);
        return newSet;
    }

    public   <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> newSet = new HashSet<>(set1);
        for (T obj2 : set2) {
            for (T obj1 : set1) {
                if (obj1.equals(obj2)) {
                    newSet.remove(obj1);
                }
            }
        }
        return newSet;
    }


}
