package homework3_6.Task4;

import java.util.ArrayList;
import java.util.List;
/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */

public class Main {
    interface A{

    }

    interface B extends A{

    }

    interface C extends A,B{

    }

    interface E extends C{

    }

    class Cl1 implements C{
    }

    class Cl2 extends Cl1 implements E{}

    public static void main(String[] args) {
        List<Class<?>> result = getAllInterfaces(Cl2.class);
        for (Class<?> anInterface : result) {
            System.out.println(anInterface.getName());

        }
    }

    public static List<Class<?>> getAllInterfaces (Class<?> cls){
        List<Class<?>> interfaces = new ArrayList<>();
         while (cls != Object.class){
             for (Class<?> anInterface : cls.getInterfaces()){
                 interfaces.add(anInterface);
                 Class<?>[] arrayInterface = anInterface.getInterfaces();
                 while (arrayInterface.length  > 0 ){
                     for (Class<?> elementInterface : arrayInterface) {
                         anInterface = elementInterface;
                         interfaces.add(anInterface);
                         arrayInterface = anInterface.getInterfaces();
                     }
                 }
             }
             cls = cls.getSuperclass();
         }
         return  interfaces;
    }
}
