package homework3_6.Task5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/*
Дана строка, состоящая из символов “(“ и “)”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Строка является правильной скобочной последовательностью, если:
● Пустая строка является правильной скобочной последовательностью.
● Пусть S — правильная скобочная последовательность, тогда (S) есть
правильная скобочная последовательность.
● Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
есть правильная скобочная последовательность
 */


public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferedReader.readLine();
        System.out.println(testingString(s));

    }

    public static boolean testingString(String s){
        if (s.equals("")) {
            return true;
        }
        if (s.length() % 2 != 0) {
            return false;
        }
        
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            String symbol = s.substring(i, i + 1);
            if (symbol.equals("(")) {
                count++;
            } else {
                count--;
            }
            if (count < 0) {
                return false;
            }

        }
        return count == 0;
    }
}
