package homework3_6.Task3;


import java.lang.reflect.Method;

/*
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */
public class Main {
    public static void main(String[] args) {
       Class <APrinter> clazz = APrinter.class;
        Method method = null;
        try {
            method = clazz.getDeclaredMethod("print", int.class);
            method.invoke(new APrinter() , 10);
        } catch (Exception e) {
            System.out.println("Error");
        }


    }

    }

