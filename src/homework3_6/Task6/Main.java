package homework3_6.Task6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Условия для правильной скобочной последовательности те, же что и в задаче 1,
но дополнительно:
● Открывающие скобки должны быть закрыты однотипными
закрывающими скобками.
● Каждой закрывающей скобке соответствует открывающая скобка того же
типа.

 */


    public class Main {
        public static void main(String[] args) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String s = bufferedReader.readLine();
            System.out.println(testingString(s));

        }

        public static boolean testingString(String s) {
            if (s.equals("")) {
                return true;
            }
            if (s.length() % 2 != 0) {
                return false;
            }

            int count = 0;
            int count2 = 0;
            int count3 = 0;

            for (int i = 0; i < s.length(); i++) {
                String symbol = s.substring(i, i + 1);
                if (symbol.equals("(") || symbol.equals("{") || symbol.equals("[")) {
                    switch (symbol) {
                        case ("(") -> count++;
                        case ("{") -> count2++;
                        case ("[") -> count3++;
                    }
                } else {
                    switch (symbol) {
                        case (")") -> count--;
                        case ("}") -> count2--;
                        case ("]") -> count3--;
                    }
                }
                if (count < 0 || count2 < 0 || count3 < 0) {
                    return false;
                }

            }
            return count == 0;
        }
    }

