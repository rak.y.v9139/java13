package homework3_6.Task2;

import homework3_6.Task1.IsLike;
/*
Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран.
 */
public class Main {
    public static void main(String[] args) {
        testingAnnotation(MyTestClass.class);
    }
    public static void testingAnnotation(Class<?> cls){
        if (!cls.isAnnotationPresent(IsLike.class)){
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println(isLike.isLike());
    }
}
