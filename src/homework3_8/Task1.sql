
/*
 Ирина, подруга Пети, решила создать свой бизнес по продаже цветов. Начать
она решила с самых основ: создать соответствующую базу данных для своего
бизнеса. Она точно знает, что будет продавать Розы по 100 золотых монет за
единицу, Лилии по 50 и Ромашки по 25.
Помимо этого, ей хочется хранить данные своих покупателей (естественно они
дали согласие на хранение персональной информации). Сохранять нужно Имя
и Номер телефона.
И, конечно, данные самого заказа тоже нужно как-то хранить! Ирина пока не
продумала поля, но она точно хочет следовать следующим правилам:
● в рамках одного заказа будет продавать только один вид цветов
(например, только розы)
● в рамках одного заказа можно купить от 1 до 1000 единиц цветов
 */


create table flowers_shop
(
    id_person    serial primary key,
    name  varchar(30) not null,
    phone varchar(30) not null,
    date  timestamp   not null
);



insert into flowers_shop (name, phone, date)
values ('Алена', '+7+900+213+34+23', now());

insert into flowers_shop (name, phone, date)
values ('Женя', '+7+900+456+45+65', now());

insert into flowers_shop (name, phone, date)
values ('Вика', '+7+900+456+45+65', now());


create table flowers
(
    id_flower      serial primary key,
    product varchar(30) not null,
    price   integer     not null
);

insert into flowers (product, price)
values ('Роза', '100');

insert into flowers (product, price)
values ('Лилии', '50');

insert into flowers (product, price)
values ('Ромашки', '25');


create table orders
(
    id_order               serial primary key,
    person           int references flowers_shop (id_person),
    flower           int references flowers (id_flower),
    quantity_product int       not null
check ( quantity_product > '0' and quantity_product <= '1000'),
    date             timestamp not null
);

insert into orders (person, flower, quantity_product, date)
values ('1', '1', '10', now());

insert into orders (person, flower, quantity_product, date)
values ('2','3','23',now());

insert into orders(person, flower, quantity_product, date)
values ('2','1','12',now());

insert into orders(person, flower, quantity_product, date)
values ('3','2','19',now());

insert into orders(person, flower, quantity_product, date)
values ('2','2','15',now());

insert into orders(person, flower, quantity_product, date)
values ('2','2','1008',now());

/*
 По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
 */

select * from orders
inner join flowers_shop
on person = id_person
where id_order = '4';


/*
 Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
 */
select * from flowers_shop
inner join orders
on id_person = orders.person
where id_person = '2' and orders.date > now() - interval '1' month ;

/*
 Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
 */

select * from orders
inner join flowers_shop
on orders.person = flowers_shop.id_person
where quantity_product = (select max(quantity_product) from orders);

/*
 Вывести общую выручку (сумму золотых монет по всем заказам) за все
время

 */

select sum(quantity_product * flowers.price)
from orders
inner join flowers
on orders.flower = flowers.id_flower;






