package homework3_3.Animal;

public class Eagle extends Animals implements Flying {
    @Override
    public void wayOfBirth() {
        System.out.println("Появляется из яйца");
    }

    @Override
    public void fly() {
        System.out.println("Быстро летает");
    }
}
