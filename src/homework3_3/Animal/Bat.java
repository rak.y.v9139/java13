package homework3_3.Animal;

public class Bat extends Animals implements Flying {
    @Override
    public void wayOfBirth() {
        System.out.println("Живородящий");
    }

    @Override
    public void fly() {
        System.out.println("Медленно летаю");
    }
}
