package homework3_3.Mebel;
/*
Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
нет. Реализовать метод в цеху, позволяющий по переданной мебели
определять, сможет ли ей починить или нет. Возвращать результат типа
boolean. Протестировать метод.
 */

public class BestCarpenterEver {
    public static void main(String[] args) {
        Table table = new Table();
        Stool stool = new Stool();
        System.out.println(
        BestCarpenterEver.repair(table));
    }
    public static boolean repair(Object object){
        return object instanceof Stool;
    }
}
