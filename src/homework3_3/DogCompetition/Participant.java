package homework3_3.DogCompetition;
import java.util.Scanner;

public class Participant {
    private String name;
    private String dogName; // Имя питомца
    public Participant() {
        Scanner scanner = new Scanner(System.in);
        this.name = scanner.next();
    }

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public String getName() {
        return name;
    }
}
