package homework3_3.DogCompetition;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Dog {
    private String dogName;
    private String participantName;
    private final List<Integer> scores = new ArrayList<>(); // Массив оценок рефери


    public Dog() {
        Scanner scanner = new Scanner(System.in);
        this.dogName = scanner.next();
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public String getParticipantName() {
        return participantName;
    }

    public String getDogName() {
        return dogName;
    }

    public void addScore(int scoreReferee) {  //Добавляем оценки от рефери в массив
        if (scoreReferee > 0 && scoreReferee < 100) {
            scores.add(scoreReferee);
        }
    }

    public double getAverageScore() { //Среднее арифметическое из оценок рефери
        double sum = 0;
        for (int i = 0; i < scores.size(); i++) {
            sum += scores.get(i);
        }
        return Math.floor(sum / scores.size()*100)/100;
    }


    }

