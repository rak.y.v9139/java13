package homework3_3.DogCompetition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public final class DogСompetition {
    private final List<Participant> participants = new ArrayList<>();
    private final List<Dog> dogs = new ArrayList<>();

    public void addParticipant() { //Добавляем нового участника соревнований
        Participant participant = new Participant();
        Dog dog = new Dog();

        dog.setParticipantName(participant.getName()); //Питомцу присваеваем имя участника
        participant.setDogName(dog.getDogName());//Участнику присваеваем имя питомца

        participants.add(participant); //Заносим питомца и участника в соответствующие массивы
        dogs.add(dog);
    }

    public void refereeScore() { //Добляем оценки от рефери для каждого питомца по имени
        Scanner scanner = new Scanner(System.in);
        String dogName = scanner.next();
        int referee1 = scanner.nextInt();
        int referee2 = scanner.nextInt();
        int referee3 = scanner.nextInt();
        for (Dog dog : dogs) {
            if (dog.getDogName().equalsIgnoreCase(dogName)) {
                dog.addScore(referee1);
                dog.addScore(referee2);
                dog.addScore(referee3);
            }
        }
    }


    public void bestThreeParticipant() {   //Выводим на экран трех лучших участников
        int winPlace = 3;  //Количество призовых мест
        double[] arr = new double[dogs.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = dogs.get(i).getAverageScore();
        }

        Arrays.sort(arr);  //Сортируем массив оценок

        for (int i = dogs.size() - 1; i >= dogs.size() - winPlace; i--) { //По оценкам из массива находим 3-х лучших участников
            for (Dog dog : dogs) {
                if (arr[i] == dog.getAverageScore()) {
                    System.out.println(dog.getParticipantName() + " " + dog.getDogName() + " " + dog.getAverageScore());
                }
            }
        }
    }

}


