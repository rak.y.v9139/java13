package homework3_3.ArraiList;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.

 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        Integer[] arr = new Integer[a];
        List<Integer[]> list = new ArrayList<>();

        for (int i = 0; i < b; i++) {
            list.add(arr);
        }
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < arr.length; j++) {
                list.get(i)[j] = i + j;
                System.out.print(list.get(i)[j] + " ");
            }
            System.out.println();
        }
    }
}

